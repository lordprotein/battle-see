// let gameStatus = {
// 	win: false,
// 	startGame: false,
// }

let gameSettings = {
	countModels: 4,
	elems: {
		arrowStep: document.querySelector('.step_arrow'),
	},
}

class Ships {
	constructor() {

		this['1'] = {
			count: 4,
			long: 1,
			coordDefault: [],
			coord: [],
		};
		this['2'] = {
			count: 3,
			long: 2,
			coordDefault: [],
			coord: [],
		};
		this['3'] = {
			count: 2,
			long: 3,
			coordDefault: [],
			coord: [],
		};
		this['4'] = {
			count: 1,
			long: 4,
			coordDefault: [],
			coord: [],
		};
	}

	addShip(currentShipArray) {
		const shipModel = currentShipArray.length;
		console.log(currentShipArray);
		if (this[shipModel] === undefined) {
			// alert(`Кораблей из ${shipModel} блоков не существует`);
			showErrorHint(`Слишком большой корабль:`, `Кораблей из ${shipModel} блоков не существует`);
			// this.resetShips();
			return false;
		}

		if (this[shipModel].count) {
			this[shipModel].coord.push(currentShipArray);
			this[shipModel].coordDefault.push(`${currentShipArray}`);
			this[shipModel].count -= 1;

			return true;
		}
		else {
			showErrorHint(`Превышено максимальное кол-во кораблей типа: ${this[shipModel].long}`, `Удалите лишние`);
			return false;
		}
	}




	resetShips() {
		console.log(`Reset...`);
		this['1'].count = defaultShips['1'].count;
		this['1'].coord = [];
		this['2'].count = defaultShips['2'].count;
		this['2'].coord = [];
		this['3'].count = defaultShips['3'].count;
		this['3'].coord = [];
		this['4'].count = defaultShips['4'].count;
		this['4'].coord = [];

		this['1'].coordDefault = defaultShips['1'].count;
		this['1'].coordDefault = [];
		this['2'].coordDefault = defaultShips['2'].count;
		this['2'].coordDefault = [];
		this['3'].coordDefault = defaultShips['3'].count;
		this['3'].coordDefault = [];
		this['4'].coordDefault = defaultShips['4'].count;
		this['4'].coordDefault = [];
	}



	checkCountShips() {
		const lengthLoop = gameSettings.countModels;

		for (let numModel = 1; numModel <= lengthLoop; numModel++) {
			if (this[numModel].count) {
				showErrorHint('Недостаточное кол-во кораблей', `Постройте ещё ${this[numModel].count} ${this[numModel].long}-х блочных корабля`)
				return false;
			}
		}

		return true;
	}

	deadShip(capitanShip, allCells, numShipInCoord) {
		console.warn(capitanShip.testCoord)
	}
}



class Capitan {
	constructor(name, btnReady) {
		this.name = name;
		this.ships = new Ships();
		this.waterCell = new waterCell();
		this.step = false;
		this.statusReady = false;
		this.deadCell = [];
		this.buttonReady = btnReady;
		this.statusWin = false;
	}


	checkDamage(capitanShip, numAttackedCell, elem, capitanCells) {
		const lengthLoop = capitanShip.coord.length;

		for (let numShipInCoord = 0; numShipInCoord < lengthLoop; numShipInCoord++) {
			const numCellInArrayCoords = capitanShip.coord[numShipInCoord].indexOf(numAttackedCell);

			if (numCellInArrayCoords != -1) {

				elem.classList.add('hit');
				this.deadCell.push(capitanShip.coord[numShipInCoord][numCellInArrayCoords]);
				const lastDead = capitanShip.coord[numShipInCoord][numCellInArrayCoords];
				capitanShip.coord[numShipInCoord].splice(numCellInArrayCoords, 1); //delete

				if (!capitanShip.coord[numShipInCoord].length) {
					console.log(`delete array`);
					this.ships.deadShip(capitanShip, this.waterCell.allCells, numShipInCoord);

					const ship = capitanShip.coordDefault[numShipInCoord].split(',');

					for (let elemShip of ship) {
						capitanCells[Number(elemShip)].classList.remove('hit');
						capitanCells[Number(elemShip)].classList.add('ship');
					}


				}

				console.log(capitanShip);

				return true;
			}
		}
	}

	nextStep(numCapitan) {
		if (numCapitan == 1) {
			capitanOne.step = true;
			capitanTwo.step = false;
			return;
		}
		if (numCapitan == 2) {
			capitanOne.step = false;
			capitanTwo.step = true;
			return;
		}
	}

	shot(capitan) {
		let capitanCells;
		const elem = event.target.closest('.cell');

		if (!elem || this.statusWin) return;
		if (elem.matches('.miss') || elem.matches('.hit')) return;

		let capitanShip, numNextStep, allCellsCapitanArray;

		if (capitan == 'capitanOne') {
			allCellsCapitanArray = Array.from(capitanTwo.waterCell.allCells);
			numNextStep = 2;
		}

		if (capitan == 'capitanTwo') {
			allCellsCapitanArray = Array.from(capitanOne.waterCell.allCells);
			numNextStep = 1;
		}


		const numAttackedCell = allCellsCapitanArray.findIndex((item) => { return item == elem }); //Find index attacked cell	
		const lengthLoop = gameSettings.countModels;

		for (let num = 1; num <= lengthLoop; num++) {

			if (capitan == 'capitanOne') {
				capitanShip = capitanTwo.ships[num];
				capitanCells = capitanTwo.waterCell.allCells;
			}
			if (capitan == 'capitanTwo') {
				capitanShip = capitanOne.ships[num];
				capitanCells = capitanOne.waterCell.allCells;

			}

			if (this.checkDamage(capitanShip, numAttackedCell, elem, capitanCells)) {
				this.step = true;
				return;
			}
		}

		elem.classList.add('miss');
		this.nextStep(numNextStep);
		reverseStepAnimation();

		return;
	}

	haveWin() {
		if (this.deadCell.length == 20) {
			showErrorHint(`Капитан ${this.name} одержал победу!`, 'Поздравляю');
			this.statusWin = true;
			setTimeout(() => location.reload(), 10000);
			return true;
		}
		return false;
	}
}


class waterCell {
	constructor() {
		this.wrapper = null;
		this.allCells = null;
		this.activeCells = [];
		this.testShip = []; // RENAME!
		this.fieldPlayer = null;
		// this.currenShipArray = [];
	}

	clickSelectCell() {
		const elem = event.target.closest('.cell');

		if (!elem) return;

		elem.classList.toggle('ship');
	}

	getActiveCellsArray() {
		this.activeCells = []; //Will to do in 

		const elemArray = Array.from(this.allCells),
			activeCells = this.activeCells,
			lengthLoop = elemArray.length;

		for (let i = 0; i < lengthLoop; i++) {
			if (elemArray[i].matches('.ship')) {
				activeCells.push(i);
			}
		}
	}


	hideAllActiveCells() {
		const allCells = this.allCells,
			lengthLoop = allCells.length;

		for (let i = 0; i < lengthLoop; i++) {
			if (allCells[i].matches('.ship')) {
				allCells[i].classList.remove('ship');
			}
		}
	}

	checkNeighborShips(currentShipArray) {
		console.log(`Active cells: ${currentShipArray} / Current: ${this.activeCells}`)

		const currenShipArray_set = new Set([...currentShipArray]),
			activeCells_set = new Set([...this.activeCells]),
			lengthLoop = currenShipArray_set.size;

		for (let num = 0; num < lengthLoop; num++) {
			const sidesArray = new Set([currentShipArray[num] - 12,
			currentShipArray[num] - 10,
			currentShipArray[num] + 10,
			currentShipArray[num] + 12,
			]);

			for (let side of sidesArray) {
				if (!currenShipArray_set.has(side) && activeCells_set.has(side)) {
					showErrorHint(`Корабли слижком близко друг к другу`, 'Увеличьте расстояние');
					//capitanTwo.ships.resetShips();
					return false;
				}
			}
		}

		return true;
	}

	addShipsInArray() //Add all ships to capitans
	{


		let recurs = (num) => {
			// console.log(`new loop`);
			const bottomElem = this.activeCells[num] + 10;
			const checkInTestShip_bottom = currentShipArray.indexOf(bottomElem);
			const checkInActiveCells_bottom = this.activeCells.indexOf(bottomElem);

			const rightElem = this.activeCells[num] + 1;
			const checkInTestShip_right = currentShipArray.indexOf(rightElem);
			const checkInActiveCells_right = this.activeCells.indexOf(rightElem);

			if (checkInTestShip_bottom == -1 && checkInActiveCells_bottom != -1) // Check bottom elem
			{
				currentShipArray.push(bottomElem);
				registerRightElemsLoop(bottomElem);
				registerLeftElemsLoop(bottomElem);
				recurs(checkInActiveCells_bottom);
			}

			if (checkInTestShip_right == -1 && checkInActiveCells_right != -1 && this.activeCells[num] % 10 != 9) {
				currentShipArray.push(rightElem);
				recurs(checkInActiveCells_right);
				return;
			}

			if (!(checkInTestShip_bottom == -1 && checkInActiveCells_bottom != -1) && !(checkInTestShip_right == -1 && checkInActiveCells_right != -1 && this.activeCells[num] % 10 != 9)) {
				return;
			}
		}


		let registerLeftElemsLoop = (thisElem) => {
			const leftElem = thisElem - 1;
			const checkInTestShip_left = currentShipArray.indexOf(leftElem);
			const checkInActiveCells_left = this.activeCells.indexOf(leftElem);

			if (checkInTestShip_left == -1 && checkInActiveCells_left != -1 && thisElem % 10 != 0) //Check left elem
			{
				currentShipArray.push(leftElem);
				registerBottomElemsLoop(leftElem);

				registerLeftElemsLoop(leftElem); //recursion 
			}
			else return;
		}

		let registerRightElemsLoop = (thisElem) => {
			const rightElem = thisElem + 1;
			const checkInTestShip_right = currentShipArray.indexOf(rightElem);
			const checkInActiveCells_right = this.activeCells.indexOf(rightElem);

			if (checkInTestShip_right == -1 && checkInActiveCells_right != -1 && thisElem % 10 != 9) {
				currentShipArray.push(rightElem);
				registerBottomElemsLoop(rightElem);

				registerRightElemsLoop(rightElem); //recursion
			}
			else return;
		}

		let registerBottomElemsLoop = (thisElem) => {
			let bottomElem = thisElem + 10;
			let checkInTestShip_bottom = currentShipArray.indexOf(bottomElem);
			let checkInActiveCells_bottom = this.activeCells.indexOf(bottomElem);

			if (checkInTestShip_bottom == -1 && checkInActiveCells_bottom != -1) // Check bottom elem
			{
				currentShipArray.push(bottomElem);

				registerBottomElemsLoop(bottomElem); //recursion
			}
			else return;
		}





		this.getActiveCellsArray();
		let allShipModel = new Set();
		let currentShipArray = [];

		const activeCells = this.activeCells,
			lengthLoop = activeCells.length,
			fieldPlayer = this.fieldPlayer;


		for (let num = 0; num < lengthLoop; num++) {

			if (allShipModel.has(activeCells[num])) continue; //If cell already insert - miss

			currentShipArray.push(activeCells[num]);

			recurs(num, currentShipArray);

			console.warn(`Current: ${currentShipArray}`);

			if (fieldPlayer.matches('.player-1')) {
				// if (!this.checkNeighborShips(currentShipArray)) return false; ..........................................

				if (!capitanOne.ships.addShip(currentShipArray)) return false; //Result this.currenShipArray

				console.log(capitanOne.ships);
			}

			if (fieldPlayer.matches('.player-2')) {
				// if (!this.checkNeighborShips(currentShipArray)) return false; ..........................................

				if (!capitanTwo.ships.addShip(currentShipArray)) return false; //Result this.currenShipArray

				console.log(capitanTwo.ships);
			}

			currentShipArray.forEach(function (elem, index) //Add all insert elem for CONTINUE similar    //////For .. of ..
			{
				allShipModel.add(elem);
			});

			currentShipArray = [];
		}

		return true;
	}
}










const capitanOne = new Capitan('Jhon', document.querySelector('.btn_ready'));
const capitanTwo = new Capitan('Fried', document.querySelectorAll('.btn_ready')[1]);

const defaultShips = new Ships();


capitanOne.waterCell.wrapper = document.querySelectorAll('.field-cells')[0];
capitanOne.waterCell.fieldPlayer = document.querySelector('.player-1');
capitanOne.waterCell.allCells = capitanOne.waterCell.wrapper.getElementsByClassName('cell');


capitanTwo.waterCell.wrapper = document.querySelectorAll('.field-cells')[1];
capitanTwo.waterCell.fieldPlayer = document.querySelector('.player-2');
capitanTwo.waterCell.allCells = capitanTwo.waterCell.wrapper.getElementsByClassName('cell');

capitanOne.step = true;









capitanOne.buttonReady.addEventListener('click', readyOnePlayer); //Button 1
capitanOne.waterCell.wrapper.addEventListener('click', clickForCreateShipOne); //Field click 1



function clickForCreateShipOne() {
	capitanOne.waterCell.clickSelectCell();
}


function clickForCreateShipTwo() {
	capitanTwo.waterCell.clickSelectCell();
}






function readyOnePlayer() {
	if (capitanOne.statusReady) return;

	if (!capitanOne.waterCell.addShipsInArray()) //Check neighbor
	{
		// console.log('Есть соседние');
		capitanOne.ships.resetShips();
		return;
	}

	if (!capitanOne.ships.checkCountShips())  //Check count  .If count any ship model less that default count - reset
	{
		capitanOne.ships.resetShips();
		return;
	}

	capitanOne.buttonReady.classList.add('active');
	capitanOne.waterCell.hideAllActiveCells();
	capitanOne.statusReady = true;

	capitanOne.waterCell.wrapper.removeEventListener('click', clickForCreateShipOne); //Delete Field click 1
	capitanTwo.waterCell.wrapper.addEventListener('click', clickForCreateShipTwo); //Field click 2
	capitanTwo.buttonReady.addEventListener('click', readyTwoPlayer);// Button 2
}


function readyTwoPlayer() {
	if (capitanTwo.statusReady) return;

	if (!capitanTwo.waterCell.addShipsInArray()) {
		capitanTwo.ships.resetShips();
		console.log('Ошибка в регистриации капитана 2')
		return;
	}

	if (!capitanTwo.ships.checkCountShips()) {
		capitanTwo.ships.resetShips();
		return;
	}

	capitanTwo.buttonReady.classList.add('active');
	capitanTwo.waterCell.hideAllActiveCells();
	capitanTwo.statusReady = true;

	capitanTwo.waterCell.wrapper.removeEventListener('click', clickForCreateShipTwo); //Delete Field click 2
}






const startGameButton = document.querySelector('.btn-start');
startGameButton.addEventListener('click', startGameListener);

function startGameListener() { //START
	if (!capitanOne.statusReady || !capitanTwo.statusReady) return;

	capitanOne.buttonReady.removeEventListener('click', readyOnePlayer); //Delete Button 1
	capitanTwo.buttonReady.removeEventListener('click', readyTwoPlayer); //Delete button 2
	// gameStatus.startGame = true;
	deleteDOMElement(capitanOne.buttonReady, capitanTwo.buttonReady, startGameButton);
	console.log(`Starting...`);







	capitanTwo.waterCell.wrapper.addEventListener('click', function () { //Shoot in cell
		if (capitanOne.step) {
			capitanOne.shot('capitanOne');
			capitanOne.haveWin();
		}
	});

	capitanOne.waterCell.wrapper.addEventListener('click', function () { //Shoot in cell
		if (capitanTwo.step) {
			capitanTwo.shot('capitanTwo');
			capitanTwo.haveWin();
		}
	});
}

function restart() {
	startGameButton.removeEventListener('click', startGameListener);
}


function deleteDOMElement() {
	for (let elem of arguments) {
		elem.remove();
	}

	// startGameButton.removeEventListener('click', startGameListener);////////////////////////////CopyPast
}

function showErrorHint(nameError, description) {
	const button = document.querySelector('.hint__accept');
	const elemParent = document.querySelector('.hint');
	const elemNameError = document.querySelector('.hint__title');
	const elemDescription = document.querySelector('.hint__description');

	elemNameError.textContent = nameError;
	elemDescription.textContent = description;

	elemParent.classList.add('hint--active')

	button.addEventListener('click', () => elemParent.classList.remove('hint--active'));
}


function reverseStepAnimation() {
	gameSettings.elems.arrowStep.classList.toggle('reverse');
}








const settStart = document.querySelector('.settings_start');

settStart.addEventListener('click', () => {
	const names = document.querySelectorAll('.settings_name');
	const fieldName = document.querySelectorAll('.capitan_name');
	const wind = names[1].closest('.settings').parentElement;

	const field = document.getElementsByTagName('main')[0];
	field.style.display = 'flex';

	const hint = document.querySelector('.hint');
	hint.style.display = 'block';

	const btnStartGame = document.querySelector('.btn_start');
	btnStartGame.style.display = 'block';

	for (let name of names) {
		if (!name.value) return;
	}

	for (let num = 0; num < names.length; num++) {
		fieldName[num].textContent = names[num].value;
	}

	wind.remove();
});











// function addNumberCell() {
// 	for (let i = 0; i < capitanOne.waterCell.allCells.length; i++) {
// 		capitanOne.waterCell.allCells[i].textContent = i;
// 		capitanTwo.waterCell.allCells[i].textContent = i;
// 	}
// }
// addNumberCell();

function autoCreateShips() {
	const numSet = new Set([96, 94, 92, 90, 77, 76, 74, 73, 71, 70, 56, 55, 54, 52, 51, 50, 33, 32, 31, 30]);
	for (let num of numSet) {
		capitanOne.waterCell.allCells[num].classList.add('ship');
		capitanTwo.waterCell.allCells[num].classList.add('ship');
	}
}
autoCreateShips();
// capitanOne.waterCell.allCells[].classList.add('hit');
