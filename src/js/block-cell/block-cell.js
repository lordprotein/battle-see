"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// let gameStatus = {
// 	win: false,
// 	startGame: false,
// }
var gameSettings = {
  countModels: 4,
  elems: {
    arrowStep: document.querySelector('.step_arrow')
  }
};

var Ships =
/*#__PURE__*/
function () {
  function Ships() {
    _classCallCheck(this, Ships);

    this['1'] = {
      count: 4,
      "long": 1,
      coordDefault: [],
      coord: []
    };
    this['2'] = {
      count: 3,
      "long": 2,
      coordDefault: [],
      coord: []
    };
    this['3'] = {
      count: 2,
      "long": 3,
      coordDefault: [],
      coord: []
    };
    this['4'] = {
      count: 1,
      "long": 4,
      coordDefault: [],
      coord: []
    };
  }

  _createClass(Ships, [{
    key: "addShip",
    value: function addShip(currentShipArray) {
      var shipModel = currentShipArray.length;

      if (this[shipModel] === undefined) {
        // alert(`Кораблей из ${shipModel} блоков не существует`);
        showErrorHint("\u0421\u043B\u0438\u0448\u043A\u043E\u043C \u0431\u043E\u043B\u044C\u0448\u043E\u0439 \u043A\u043E\u0440\u0430\u0431\u043B\u044C:", "\u041A\u043E\u0440\u0430\u0431\u043B\u0435\u0439 \u0438\u0437 ".concat(shipModel, " \u0431\u043B\u043E\u043A\u043E\u0432 \u043D\u0435 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u0443\u0435\u0442")); // this.resetShips();

        return false;
      }

      if (this[shipModel].count) {
        this[shipModel].coord.push(currentShipArray);
        this[shipModel].coordDefault.push("".concat(currentShipArray));
        this[shipModel].count -= 1;
        return true;
      } else {
        showErrorHint("\u041F\u0440\u0435\u0432\u044B\u0448\u0435\u043D\u043E \u043C\u0430\u043A\u0441\u0438\u043C\u0430\u043B\u044C\u043D\u043E\u0435 \u043A\u043E\u043B-\u0432\u043E \u043A\u043E\u0440\u0430\u0431\u043B\u0435\u0439 \u0442\u0438\u043F\u0430: ".concat(this[shipModel]["long"]), "\u0423\u0434\u0430\u043B\u0438\u0442\u0435 \u043B\u0438\u0448\u043D\u0438\u0435");
        return false;
      }
    }
  }, {
    key: "resetShips",
    value: function resetShips() {
      console.log("Reset...");
      this['1'].count = defaultShips['1'].count;
      this['1'].coord = [];
      this['2'].count = defaultShips['2'].count;
      this['2'].coord = [];
      this['3'].count = defaultShips['3'].count;
      this['3'].coord = [];
      this['4'].count = defaultShips['4'].count;
      this['4'].coord = [];
      this['1'].coordDefault = defaultShips['1'].count;
      this['1'].coordDefault = [];
      this['2'].coordDefault = defaultShips['2'].count;
      this['2'].coordDefault = [];
      this['3'].coordDefault = defaultShips['3'].count;
      this['3'].coordDefault = [];
      this['4'].coordDefault = defaultShips['4'].count;
      this['4'].coordDefault = [];
    }
  }, {
    key: "checkCountShips",
    value: function checkCountShips() {
      var lengthLoop = gameSettings.countModels;

      for (var numModel = 1; numModel <= lengthLoop; numModel++) {
        if (this[numModel].count) {
          showErrorHint('Недостаточное кол-во кораблей', "\u041F\u043E\u0441\u0442\u0440\u043E\u0439\u0442\u0435 \u0435\u0449\u0451 ".concat(this[numModel].count, " ").concat(this[numModel]["long"], "-\u0445 \u0431\u043B\u043E\u0447\u043D\u044B\u0445 \u043A\u043E\u0440\u0430\u0431\u043B\u044F"));
          return false;
        }
      }

      return true;
    }
  }, {
    key: "deadShip",
    value: function deadShip(capitanShip, allCells, numShipInCoord) {
      console.warn(capitanShip.testCoord);
    }
  }]);

  return Ships;
}();

var Capitan =
/*#__PURE__*/
function () {
  function Capitan(name, btnReady) {
    _classCallCheck(this, Capitan);

    this.name = name;
    this.ships = new Ships();
    this.waterCell = new waterCell();
    this.step = false;
    this.statusReady = false;
    this.deadCell = [];
    this.buttonReady = btnReady;
    this.statusWin = false;
  }

  _createClass(Capitan, [{
    key: "checkDamage",
    value: function checkDamage(capitanShip, numAttackedCell, elem, capitanCells) {
      var lengthLoop = capitanShip.coord.length;

      for (var numShipInCoord = 0; numShipInCoord < lengthLoop; numShipInCoord++) {
        var numCellInArrayCoords = capitanShip.coord[numShipInCoord].indexOf(numAttackedCell);

        if (numCellInArrayCoords != -1) {
          elem.classList.add('hit');
          this.deadCell.push(capitanShip.coord[numShipInCoord][numCellInArrayCoords]);
          var lastDead = capitanShip.coord[numShipInCoord][numCellInArrayCoords];
          capitanShip.coord[numShipInCoord].splice(numCellInArrayCoords, 1); //delete

          if (!capitanShip.coord[numShipInCoord].length) {
            console.log("delete array");
            this.ships.deadShip(capitanShip, this.waterCell.allCells, numShipInCoord);
            var ship = capitanShip.coordDefault[numShipInCoord].split(',');
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
              for (var _iterator = ship[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var elemShip = _step.value;
                capitanCells[Number(elemShip)].classList.remove('hit');
                capitanCells[Number(elemShip)].classList.add('ship');
              }
            } catch (err) {
              _didIteratorError = true;
              _iteratorError = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion && _iterator["return"] != null) {
                  _iterator["return"]();
                }
              } finally {
                if (_didIteratorError) {
                  throw _iteratorError;
                }
              }
            }
          }

          console.log(capitanShip);
          return true;
        }
      }
    }
  }, {
    key: "nextStep",
    value: function nextStep(numCapitan) {
      if (numCapitan == 1) {
        capitanOne.step = true;
        capitanTwo.step = false;
        return;
      }

      if (numCapitan == 2) {
        capitanOne.step = false;
        capitanTwo.step = true;
        return;
      }
    }
  }, {
    key: "shot",
    value: function shot(capitan) {
      var capitanCells;
      var elem = event.target.closest('.cell');
      if (!elem || this.statusWin) return;
      if (elem.matches('.miss') || elem.matches('.hit')) return;
      var capitanShip, numNextStep, allCellsCapitanArray;

      if (capitan == 'capitanOne') {
        allCellsCapitanArray = Array.from(capitanTwo.waterCell.allCells);
        numNextStep = 2;
      }

      if (capitan == 'capitanTwo') {
        allCellsCapitanArray = Array.from(capitanOne.waterCell.allCells);
        numNextStep = 1;
      }

      var numAttackedCell = allCellsCapitanArray.findIndex(function (item) {
        return item == elem;
      }); //Find index attacked cell	

      var lengthLoop = gameSettings.countModels;

      for (var num = 1; num <= lengthLoop; num++) {
        if (capitan == 'capitanOne') {
          capitanShip = capitanTwo.ships[num];
          capitanCells = capitanTwo.waterCell.allCells;
        }

        if (capitan == 'capitanTwo') {
          capitanShip = capitanOne.ships[num];
          capitanCells = capitanOne.waterCell.allCells;
        }

        if (this.checkDamage(capitanShip, numAttackedCell, elem, capitanCells)) {
          this.step = true;
          return;
        }
      }

      elem.classList.add('miss');
      this.nextStep(numNextStep);
      reverseStepAnimation();
      return;
    }
  }, {
    key: "haveWin",
    value: function haveWin() {
      if (this.deadCell.length == 20) {
        showErrorHint("\u041A\u0430\u043F\u0438\u0442\u0430\u043D ".concat(this.name, " \u043E\u0434\u0435\u0440\u0436\u0430\u043B \u043F\u043E\u0431\u0435\u0434\u0443!"), 'Поздравляю');
        this.statusWin = true;
        setTimeout(function () {
          return location.reload();
        }, 10000);
        return true;
      }

      return false;
    }
  }]);

  return Capitan;
}();

var waterCell =
/*#__PURE__*/
function () {
  function waterCell() {
    _classCallCheck(this, waterCell);

    this.wrapper = null;
    this.allCells = null;
    this.activeCells = [];
    this.testShip = []; // RENAME!

    this.fieldPlayer = null; // this.currenShipArray = [];
  }

  _createClass(waterCell, [{
    key: "clickSelectCell",
    value: function clickSelectCell() {
      var elem = event.target.closest('.cell');
      if (!elem) return;
      elem.classList.toggle('ship');
    }
  }, {
    key: "getActiveCellsArray",
    value: function getActiveCellsArray() {
      this.activeCells = []; //Will to do in 

      var elemArray = Array.from(this.allCells),
          activeCells = this.activeCells,
          lengthLoop = elemArray.length;

      for (var i = 0; i < lengthLoop; i++) {
        if (elemArray[i].matches('.ship')) {
          activeCells.push(i);
        }
      }
    }
  }, {
    key: "hideAllActiveCells",
    value: function hideAllActiveCells() {
      var allCells = this.allCells,
          lengthLoop = allCells.length;

      for (var i = 0; i < lengthLoop; i++) {
        if (allCells[i].matches('.ship')) {
          allCells[i].classList.remove('ship');
        }
      }
    }
  }, {
    key: "checkNeighborShips",
    value: function checkNeighborShips(currentShipArray) {
      console.log("Active cells: ".concat(currentShipArray, " / Current: ").concat(this.activeCells));
      var currenShipArray_set = new Set(_toConsumableArray(currentShipArray)),
          activeCells_set = new Set(_toConsumableArray(this.activeCells)),
          lengthLoop = currenShipArray_set.size;

      for (var num = 0; num < lengthLoop; num++) {
        var sidesArray = new Set([currentShipArray[num] - 12, currentShipArray[num] - 10, currentShipArray[num] + 10, currentShipArray[num] + 12]);
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
          for (var _iterator2 = sidesArray[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            var side = _step2.value;

            if (!currenShipArray_set.has(side) && activeCells_set.has(side)) {
              showErrorHint("\u041A\u043E\u0440\u0430\u0431\u043B\u0438 \u0441\u043B\u0438\u0436\u043A\u043E\u043C \u0431\u043B\u0438\u0437\u043A\u043E \u0434\u0440\u0443\u0433 \u043A \u0434\u0440\u0443\u0433\u0443", 'Увеличьте расстояние'); //capitanTwo.ships.resetShips();

              return false;
            }
          }
        } catch (err) {
          _didIteratorError2 = true;
          _iteratorError2 = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
              _iterator2["return"]();
            }
          } finally {
            if (_didIteratorError2) {
              throw _iteratorError2;
            }
          }
        }
      }

      return true;
    }
  }, {
    key: "addShipsInArray",
    value: function addShipsInArray() //Add all ships to capitans
    {
      var _this = this;

      var recurs = function recurs(num) {
        // console.log(`new loop`);
        var bottomElem = _this.activeCells[num] + 11;
        var checkInTestShip_bottom = currentShipArray.indexOf(bottomElem);

        var checkInActiveCells_bottom = _this.activeCells.indexOf(bottomElem);

        var rightElem = _this.activeCells[num] + 1;
        var checkInTestShip_right = currentShipArray.indexOf(rightElem);

        var checkInActiveCells_right = _this.activeCells.indexOf(rightElem);

        if (checkInTestShip_bottom == -1 && checkInActiveCells_bottom != -1) // Check bottom elem
          {
            currentShipArray.push(bottomElem);
            registerRightElemsLoop(bottomElem);
            registerLeftElemsLoop(bottomElem);
            recurs(checkInActiveCells_bottom);
          }

        if (checkInTestShip_right == -1 && checkInActiveCells_right != -1) {
          currentShipArray.push(rightElem);
          recurs(checkInActiveCells_right);
          return;
        }

        if (!(checkInTestShip_bottom == -1 && checkInActiveCells_bottom != -1) && !(checkInTestShip_right == -1 && checkInActiveCells_right != -1)) {
          return;
        }
      };

      var registerLeftElemsLoop = function registerLeftElemsLoop(thisElem) {
        var leftElem = thisElem - 1;
        var checkInTestShip_left = currentShipArray.indexOf(leftElem);

        var checkInActiveCells_left = _this.activeCells.indexOf(leftElem);

        if (checkInTestShip_left == -1 && checkInActiveCells_left != -1) //Check left elem
          {
            currentShipArray.push(leftElem);
            registerBottomElemsLoop(leftElem);
            registerLeftElemsLoop(leftElem); //recursion 
          } else return;
      };

      var registerRightElemsLoop = function registerRightElemsLoop(thisElem) {
        var rightElem = thisElem + 1;
        var checkInTestShip_right = currentShipArray.indexOf(rightElem);

        var checkInActiveCells_right = _this.activeCells.indexOf(rightElem);

        if (checkInTestShip_right == -1 && checkInActiveCells_right != -1) {
          currentShipArray.push(rightElem);
          registerBottomElemsLoop(rightElem);
          registerRightElemsLoop(rightElem); //recursion
        } else return;
      };

      var registerBottomElemsLoop = function registerBottomElemsLoop(thisElem) {
        var bottomElem = thisElem + 11;
        var checkInTestShip_bottom = currentShipArray.indexOf(bottomElem);

        var checkInActiveCells_bottom = _this.activeCells.indexOf(bottomElem);

        if (checkInTestShip_bottom == -1 && checkInActiveCells_bottom != -1) // Check bottom elem
          {
            currentShipArray.push(bottomElem);
            registerBottomElemsLoop(bottomElem); //recursion
          } else return;
      };

      this.getActiveCellsArray();
      var allShipModel = new Set();
      var currentShipArray = [];
      var activeCells = this.activeCells,
          lengthLoop = activeCells.length,
          fieldPlayer = this.fieldPlayer;

      for (var num = 0; num < lengthLoop; num++) {
        if (allShipModel.has(activeCells[num])) continue; //If cell already insert - miss

        currentShipArray.push(activeCells[num]);
        recurs(num, currentShipArray);
        console.warn("Current: ".concat(currentShipArray));

        if (fieldPlayer.matches('.player-1')) {
          if (!this.checkNeighborShips(currentShipArray)) return false;
          if (!capitanOne.ships.addShip(currentShipArray)) return false; //Result this.currenShipArray

          console.log(capitanOne.ships);
        }

        if (fieldPlayer.matches('.player-2')) {
          if (!this.checkNeighborShips(currentShipArray)) return false;
          if (!capitanTwo.ships.addShip(currentShipArray)) return false; //Result this.currenShipArray

          console.log(capitanTwo.ships);
        }

        currentShipArray.forEach(function (elem, index) //Add all insert elem for CONTINUE similar    //////For .. of ..
        {
          allShipModel.add(elem);
        });
        currentShipArray = [];
      }

      return true;
    }
  }]);

  return waterCell;
}();

var capitanOne = new Capitan('Jhon', document.querySelector('.btn_ready'));
var capitanTwo = new Capitan('Fried', document.querySelectorAll('.btn_ready')[1]);
var defaultShips = new Ships();
capitanOne.waterCell.wrapper = document.querySelectorAll('.field-cells')[0];
capitanOne.waterCell.fieldPlayer = document.querySelector('.player-1');
capitanOne.waterCell.allCells = capitanOne.waterCell.wrapper.getElementsByClassName('cell');
capitanTwo.waterCell.wrapper = document.querySelectorAll('.field-cells')[1];
capitanTwo.waterCell.fieldPlayer = document.querySelector('.player-2');
capitanTwo.waterCell.allCells = capitanTwo.waterCell.wrapper.getElementsByClassName('cell');
capitanOne.step = true;
capitanOne.buttonReady.addEventListener('click', readyOnePlayer); //Button 1

capitanOne.waterCell.wrapper.addEventListener('click', clickForCreateShipOne); //Field click 1

function clickForCreateShipOne() {
  capitanOne.waterCell.clickSelectCell();
}

function clickForCreateShipTwo() {
  capitanTwo.waterCell.clickSelectCell();
}

function readyOnePlayer() {
  if (capitanOne.statusReady) return;

  if (!capitanOne.waterCell.addShipsInArray()) //Check neighbor
    {
      // console.log('Есть соседние');
      capitanOne.ships.resetShips();
      return;
    }

  if (!capitanOne.ships.checkCountShips()) //Check count  .If count any ship model less that default count - reset
    {
      capitanOne.ships.resetShips();
      return;
    }

  capitanOne.buttonReady.classList.add('active');
  capitanOne.waterCell.hideAllActiveCells();
  capitanOne.statusReady = true;
  capitanOne.waterCell.wrapper.removeEventListener('click', clickForCreateShipOne); //Delete Field click 1

  capitanTwo.waterCell.wrapper.addEventListener('click', clickForCreateShipTwo); //Field click 2

  capitanTwo.buttonReady.addEventListener('click', readyTwoPlayer); // Button 2
}

function readyTwoPlayer() {
  if (capitanTwo.statusReady) return;

  if (!capitanTwo.waterCell.addShipsInArray()) {
    capitanTwo.ships.resetShips();
    console.log('Ошибка в регистриации капитана 2');
    return;
  }

  if (!capitanTwo.ships.checkCountShips()) {
    capitanTwo.ships.resetShips();
    return;
  }

  capitanTwo.buttonReady.classList.add('active');
  capitanTwo.waterCell.hideAllActiveCells();
  capitanTwo.statusReady = true;
  capitanTwo.waterCell.wrapper.removeEventListener('click', clickForCreateShipTwo); //Delete Field click 2
}

var startGameButton = document.querySelector('.btn-start');
startGameButton.addEventListener('click', startGameListener);

function startGameListener() {
  //START
  if (!capitanOne.statusReady || !capitanTwo.statusReady) return;
  capitanOne.buttonReady.removeEventListener('click', readyOnePlayer); //Delete Button 1

  capitanTwo.buttonReady.removeEventListener('click', readyTwoPlayer); //Delete button 2
  // gameStatus.startGame = true;

  deleteDOMElement(capitanOne.buttonReady, capitanTwo.buttonReady, startGameButton);
  console.log("Starting...");
  capitanTwo.waterCell.wrapper.addEventListener('click', function () {
    //Shoot in cell
    if (capitanOne.step) {
      capitanOne.shot('capitanOne');
      capitanOne.haveWin();
    }
  });
  capitanOne.waterCell.wrapper.addEventListener('click', function () {
    //Shoot in cell
    if (capitanTwo.step) {
      capitanTwo.shot('capitanTwo');
      capitanTwo.haveWin();
    }
  });
}

function restart() {
  startGameButton.removeEventListener('click', startGameListener);
}

function deleteDOMElement() {
  var _iteratorNormalCompletion3 = true;
  var _didIteratorError3 = false;
  var _iteratorError3 = undefined;

  try {
    for (var _iterator3 = arguments[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
      var elem = _step3.value;
      elem.remove();
    } // startGameButton.removeEventListener('click', startGameListener);////////////////////////////CopyPast

  } catch (err) {
    _didIteratorError3 = true;
    _iteratorError3 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion3 && _iterator3["return"] != null) {
        _iterator3["return"]();
      }
    } finally {
      if (_didIteratorError3) {
        throw _iteratorError3;
      }
    }
  }
}

function showErrorHint(nameError, description) {
  var button = document.querySelector('.hint__accept');
  var elemParent = document.querySelector('.hint');
  var elemNameError = document.querySelector('.hint__title');
  var elemDescription = document.querySelector('.hint__description');
  elemNameError.textContent = nameError;
  elemDescription.textContent = description;
  elemParent.classList.add('hint--active');
  button.addEventListener('click', function () {
    return elemParent.classList.remove('hint--active');
  });
}

function reverseStepAnimation() {
  gameSettings.elems.arrowStep.classList.toggle('reverse');
}

var settStart = document.querySelector('.settings_start');
settStart.addEventListener('click', function () {
  var names = document.querySelectorAll('.settings_name');
  var fieldName = document.querySelectorAll('.capitan_name');
  var wind = names[1].closest('.settings').parentElement;
  var field = document.getElementsByTagName('main')[0];
  field.style.display = 'flex';
  var hint = document.querySelector('.hint');
  hint.style.display = 'block';
  var btnStartGame = document.querySelector('.btn_start');
  btnStartGame.style.display = 'block';
  var _iteratorNormalCompletion4 = true;
  var _didIteratorError4 = false;
  var _iteratorError4 = undefined;

  try {
    for (var _iterator4 = names[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
      var name = _step4.value;
      if (!name.value) return;
    }
  } catch (err) {
    _didIteratorError4 = true;
    _iteratorError4 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion4 && _iterator4["return"] != null) {
        _iterator4["return"]();
      }
    } finally {
      if (_didIteratorError4) {
        throw _iteratorError4;
      }
    }
  }

  for (var num = 0; num < names.length; num++) {
    fieldName[num].textContent = names[num].value;
  }

  wind.remove();
}); // function addNumberCell()
// {
// 	for(let i = 0; i < capitanOne.waterCell.allCells.length; i++)
// 	{
// 		capitanOne.waterCell.allCells[i].textContent = i;
// 		capitanTwo.waterCell.allCells[i].textContent = i;
// 	}
// }
// addNumberCell();

function autoCreateShips() {
  var numSet = new Set([96, 94, 92, 90, 77, 76, 74, 73, 71, 70, 56, 55, 54, 52, 51, 50, 33, 32, 31, 30]);
  var _iteratorNormalCompletion5 = true;
  var _didIteratorError5 = false;
  var _iteratorError5 = undefined;

  try {
    for (var _iterator5 = numSet[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
      var num = _step5.value;
      capitanOne.waterCell.allCells[num].classList.add('ship');
      capitanTwo.waterCell.allCells[num].classList.add('ship');
    }
  } catch (err) {
    _didIteratorError5 = true;
    _iteratorError5 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion5 && _iterator5["return"] != null) {
        _iterator5["return"]();
      }
    } finally {
      if (_didIteratorError5) {
        throw _iteratorError5;
      }
    }
  }
}

autoCreateShips(); // capitanOne.waterCell.allCells[].classList.add('hit');