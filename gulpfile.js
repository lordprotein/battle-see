var gulp  = require('gulp'),
	less  = require('gulp-less'),
	babel = require('gulp-babel'),
	autoprefixer_css = require('gulp-autoprefixer');

var src = {
	blocks: 'src/blocks',
	styles: 'src/styles',
	js: 'src/js',
}

function less_compile(done) {
	gulp.src(src.styles + '/styles.less')
	.pipe(less())
	.pipe(autoprefixer_css({
            browsers: ['last 100 versions', 'ie 8', 'ie 9'],
            cascade: true
        }))
	.pipe(gulp.dest(src.styles))
	done();
}

function babel_compile(done) {
	gulp.src(src.blocks + '/**/*.js')
	.pipe(babel({
		presets: ['@babel/env']
	}))
	.on('error', function (err) { console.log(err.message); })
	.pipe(gulp.dest(src.js))
	done();
}


gulp.task('less', less_compile);
gulp.task('babel', babel_compile);

gulp.task('watch', function() {
	gulp.watch(src.blocks + '/**/*.less', gulp.parallel('less'));
	gulp.watch(src.blocks + '/**/*.js', gulp.parallel('babel'));
});
